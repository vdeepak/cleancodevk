# Clean Code 

## Introduction 

This repository contains all the best practices one should follow when writing code. This repository has the summary of the book
[Clean Code by Robert C. Martin](https://www.goodreads.com/book/show/3735293-clean-code)

## Usage 

Every class has guidelines for writing that particular concept.
For Example `EmployeeClass` has guidelines on how to write Class and related concepts, and `EmployeeFunction` has guidelines on how to write Functions/Methods. And so on...